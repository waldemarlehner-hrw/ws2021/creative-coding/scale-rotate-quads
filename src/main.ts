import './style.css'
import P5 from "p5";
import ScaleRotateQuadsSketch from "./ScaleRotateQuadsSketch"

const sketch = ScaleRotateQuadsSketch.toP5Sketch();

new P5(sketch);
