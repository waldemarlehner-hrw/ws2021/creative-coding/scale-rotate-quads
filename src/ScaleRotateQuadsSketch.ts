import p5 from "p5";

class ScaleRotateQuadsSketch {
	
	private margin = 50;
	private xCount = 30;
	private yCount = 20;
	private maxAmplitude = 100;
	private rectangleSize = 30;
	private speed = 0.05;


	private counter = 0;
	private animationMethod?: "STATIC" | "VERTICAL" | "DIAGONAL";
	private transformations: {rotation: number, vector: {x: number, y: number}, scale: number}[] = [];

	public get width(){
		return this.xCount * this.rectangleSize + 2* this.margin;
	}
	public get height(){
		return this.yCount * this.rectangleSize + 2* this.margin;
	}
	
	
	private canvas?: p5.Renderer;
	

	constructor(private p: p5){
		p.setup = () => this.setup();
		p.draw = () => this.draw();
	}

	private setup() {
		this.animationMethod = this.getAnimationMethod(window?.location?.hash?.toLocaleLowerCase() ?? "");

		this.canvas = this.p.createCanvas(this.width, this.height);
		this.canvas.parent("app");
		this.p.noFill();

		for(let y = 0; y < this.yCount; y++) {
			const fraction = y / this.yCount;
			for(let x = 0; x < this.xCount; x++) {
				// Starts Slowly, chaos starts abruply
				const multiplier = fraction * fraction ;
				const vector = {x: multiplier * Math.random() * this.maxAmplitude, y: multiplier * Math.random() * this.maxAmplitude};
				const rotation = Math.random() * multiplier;
				const scale = Math.random() * multiplier;
				this.transformations.push({rotation, vector, scale});
			}
		}

	}
	getAnimationMethod(hash: string) {
		console.log(hash)
		switch (hash) {
			case "#vertical":
				return "VERTICAL";
			case "#diagonal":
				return "DIAGONAL";
			default:
				return "STATIC";
		} 

	}

	private draw() {
		this.p.background("#eaeaea");
		for(let y = 0; y < this.yCount; y++) {
			for(let x = 0; x < this.xCount; x++) {
				let value: number;
				switch(this.animationMethod) {
					case "DIAGONAL":
						value = Math.sin((this.counter + x + y ) * this.speed); break;
					case "VERTICAL":
						value = Math.sin((this.counter  + x) * this.speed); break;
					case "STATIC":
					default:
						value = Math.sin((this.counter) * this.speed); break;	
				}
				const transformation = this.transformations[y * this.yCount + x]

				const scale = 1 - (y/this.yCount)-((value + 1) / 2) * transformation.scale;
				const rectSize = this.rectangleSize * scale * value;
				this.p.push()
				this.p.translate(
					transformation.vector.x * value + this.margin + x * this.rectangleSize - .5*rectSize, 
					transformation.vector.y * value + this.margin + y * this.rectangleSize - .5*rectSize
					);
				this.p.rotate(transformation.rotation * value)
				this.p.rect(0,0, rectSize, rectSize)
				this.p.pop()
			}
		}
		this.counter++;
	}
	
	public static toP5Sketch(): (p5: p5) => void {
		return (p5: p5) => new ScaleRotateQuadsSketch(p5);
	}
}

export default ScaleRotateQuadsSketch;
